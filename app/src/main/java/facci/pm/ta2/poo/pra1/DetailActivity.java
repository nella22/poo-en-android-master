package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView precio, descripcion1, nombre;
    ImageView imagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        precio = (TextView)findViewById(R.id.precio);
        descripcion1 = (TextView)findViewById(R.id.descripcion1);
        nombre = (TextView)findViewById(R.id.nombre);

        imagen = (ImageView)findViewById(R.id.thumbnail);


        final DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("prueba");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){

                    String prec = (String) object.get("price");
                    String descrip = (String) object.get("description");
                    String nom = (String) object.get("name");
                    Bitmap bitmap = (Bitmap) object.get("image");

                    precio.setText("$"+prec);
                    precio.setTextColor(getColor(R.color.precioRojo));
                    descripcion1.setText(descrip);
                    nombre.setText(nom);
                    imagen.setImageBitmap(bitmap);

                }else {
                    //error
                }
            }
        });




    }

}
